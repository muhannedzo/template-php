<?php
include_once('include/layout/header.php');
include_once('include/connection.php');
?>
<body>
    <section class="intro" id="intro">
        <div class="container   ">
            <div class="align-items-center row ">
                <div class="col-12 d-none d-lg-block  px-5 mt-20">
                    <div class="intro-text  text-white m-auto bg-dark-opacity">
                        <h1 class="yellow ">Let's Build A Legacy</h1>
                        <p class="">Connecting People & Property, Perfectly.Helping Hundreds Of People Move Home Every Year In Abu Dhabi.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="popular-communities mt-5 wow fadeInRight" id="communities">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Popular Communities</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="slider">
                    <?php
                        $sql = "select * from popular_communities;";
                        $result = mysqli_query($conn, $sql);
                        $resultCheck = mysqli_num_rows($result);
                        if($resultCheck > 0){
                            while($row = mysqli_fetch_assoc($result)){
                                $img= $row["image_url"];
                                echo "<div class='card card-popular m-2'>
                                    <div class='card-image card-img-top'>
                                        <img src='$img' width='100%' height='auto' alt=''>
                                    </div>
                                    <div class='card-title text-center'>
                                        ".$row['title']."
                                    </div>
                                </div>";        
                            }
                        } 
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="rental my-5" id="developers">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        Featured Developers
                    </h2>
                    <p class="divider m-auto"></p>
                    <p class="mt-3 gray fs-18">
                        Our residential, retail, and commercial projects all have something in common. They aspire to stimulate immediate surroundings and enrich the lives of the people that interact with them.
                    </p>
                </div>
            </div>
            <div class="row">
                <?php
                    $sql = "select * from featured_developers";
                    $result = mysqli_query($conn, $sql);
                    $resultCheck = mysqli_num_rows($result);
                    if($resultCheck > 0){
                        while($row = mysqli_fetch_assoc($result)){ 
                            $img = $row['image_url'];
                        echo  "<div class='col-12 col-md-6 col-lg-4 my-4'>
                                <div class='card text-center wow fadeInUp custom-card'>
                                    <img class='card-img-top' src='$img' alt='".$row['title']."'>
                                    <h3 class='card-title'>".$row['title']."</h3>
                                    <div class='custom-card-body'>
                                        <div class='card-description m-2' style='height:18vh;'>
                                            <p>".$row['description']."</p>
                                        </div>
                                        <div class='card-btns'>
                                            <div class='row'>
                                                <div class='col-6'>
                                                    <button class='btn btn-warning card-btn whats-btn'>
                                                    <a href='https://api.whatsapp.com/send?phone=024466775&text=Hi,'>    
                                                    <span><i class='fa fa-whatsapp c-icon'></i> WhatsApp</span>
                                                    </a>
                                                    </button>
                                                </div>
                                                <div class='col-6'>
                                                    <button class='btn btn-warning card-btn call-btn'><a href='tel:024466775'><span><i class='fa fa-phone c-icon'></i> Call Us</a></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>";
                        }
                    }
                ?>
            </div>
        </div>
    </div>
    </section>
        <!--    for web-->
        <section class="overflow-hidden d-none d-md-block">
        <div class="container p-0 p-md-3">
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 p-md-3 p-0">
                    <img src="assets/img/12947.jpg" alt="Chauffeur Service with Driver"
                         class="w-100 zoom">
                </div>
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="  pt-4 pl-4   bg-blue">
                        <div class="border-top-left-3 pt-1 pl-1">
                            <div class="border-top-left pt-1 pl-1">
                                <h2 class="text-white px-3 pt-3 wow fadeInLeft">FEATURED DEVELOPMENTS</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-3 pb-4 wow fadeInLeft">Development began in 2006 with the aim of creating an integrated community for leisure, shopping and more. Also, the development of the island as a suitable place for outdoor activities, such as sailing by auction, sunset at sunset, or easy and convenient shopping.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row px-md-3 px-0 py-3">
                <div class="col-12 col-md-6 px-0  m-auto">
                    <div class="pt-4 pr-4  bg-orange">
                        <div class="border-top-right-3 pt-1 pr-1">
                            <div class="border-top-right pt-1 pr-1">
                                <h2 class="text-white px-4 pt-3 wow fadeInRight ">FEATURED DEVELOPMENTS</h2>

                                <p class="text-white mt-4 mb-0 fs-12 px-4 pb-4 wow fadeInRight ">Look at our Latest listed properties and check out the facilities on them, We have already sold more than 5,000 Homes and we are still going at very good pace.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 p-md-3 p-0">
                    <img src="assets/img/New-Project-2021-08-30T124943.884.jpg" alt="Limousin Service" class="w-100 zoom">
                </div>
            </div>

        </div>
    </section>
    <!--    end for web-->
    <!--    for mobile-->
    <section class="overflow-hidden d-block d-md-none">
        <div class="container">
            <div class="row">
                <div class="col-12 by-3 my-3">
                    <div class="service-blue-bg wow fadeInUp">
                        <h2>
                            FEATURED DEVELOPMENTS
                        </h2>
                        <p class="three-line see-more-1" data-type="close">
                            Development began in 2006 with the aim of creating an integrated community for leisure, shopping and more. Also, the development of the island as a suitable place for outdoor activities, such as sailing by auction, sunset at sunset, or easy and convenient shopping.
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="1">Read More</span>
                        </div>
                    </div>
                </div>
                <div class="col-12 by-3 my-3">
                    <div class="service-orange-bg wow fadeInUp">
                        <h2>
                            FEATURED DEVELOPMENTS
                        </h2>
                        <p class="three-line see-more-2" data-type="close">
                            Look at our Latest listed properties and check out the facilities on them, We have already sold more than 5,000 Homes and we are still going at very good pace.
                        </p>
                        <div class="text-right">
                            <span class="yellow read-more" data-target="2">Read More</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    end for mobile-->
    <section class="mb-5" id="media">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        Our Media
                    </h2>
                    <p class="divider m-auto"></p>
                </div>
            </div>        
            <div class="row mt-5">
                <div class="col-12 col-md-6 mt-2 wow fadeInUp">
                    <iframe width="100%" height="350" src="https://www.youtube.com/embed/GBaBi9RUoSk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="col col-md-6 mt-2 wow fadeInDown">
                    <iframe width="100%" height="350" src="https://www.youtube.com/embed/wr8lzt1VVkc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-5" id="brands">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="slike">
                        <?php
                            $sql = "select * from top_developers;";
                            $result = mysqli_query($conn, $sql);
                            $resultCheck = mysqli_num_rows($result);
                            if($resultCheck > 0){
                                while($row = mysqli_fetch_assoc($result)){
                                    $img = $row['image_url'];
                                    echo "<div class='p-2 p-md-5'>
                                        <img src='$img' alt='Audi' class='w-100'>
                                    </div>";
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-5" id="scrollable-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gray scrollable-text">
                        <div class="text-center"><p class="fs-40" style="font-weight: bold">Welcome To The Reality</p></div>
                        <h2>Pure Home Real Estate</h2>
                            <p>What is Lorem Ipsum?
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

                            Why do we use it?
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).


                            Where does it come from?
                            Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

                            The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. </p>
                    </div>
                </div>
            </div>
    </section>
    <section class="booking" id="booking">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-5">
                    <h2 class="text-center wow fadeInUp">Contact Us</h2>
                    <p class="divider m-auto"></p>
                </div>
                <div class="col-12 px-5 pb-4  text-center wow fadeInDown">
                    <div class="mt-65">
                        <form class="text-left text-white" id="bookForm1" data-parsley-validate>
                            <div class="bg-dark-opacity px-5 py-3 ">
                                <div class="form-row b-white-border ">
                                    <div class="form-group col-12 col-md-4 r-gray-border">
                                        <label class="label-form" for="name1">Name <sup>*</sup></label>
                                        <input type="text" class="form-control opacity-form" id="name1"
                                               placeholder="Your Name" required="">
                                    </div>
                                    <div class="form-group col-12 col-md-4 r-gray-border">
                                        <label class="label-form w-100" for="phone1">Phone <sup>*</sup></label>
                                        <input type="tel" class="form-control opacity-form phone" id="phone1"
                                               placeholder="Your Number" required=""
                                               data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$"
                                        >
                                    </div>
                                    <div class="form-group col-12 col-md-4">
                                        <label class="label-form" for="email1">Email
                                            <sup>*</sup></label>
                                        <input type="email" class="form-control opacity-form email" id="email1"
                                            placeholder="Your Email" required=""> 
                                    </div>
                                </div>
                                <div class="form-group mt-2">
                                    <label class="label-form" for="message1">Your Message</label>
                                    <textarea id="message1" class="form-control opacity-form" placeholder="Message"
                                              rows="1"></textarea>
                                </div>
                            </div>
                            <div class="text-center w-100">
                                <div class="contact-error alert alert-danger mt-3">Try Again Later</div>
                                <div class="contact-success alert alert-success mt-3">Submitted. We will get back to you
                                    shortly.
                                </div>
                                <button type="submit" class="btn btn-warning mt-3 card-btn">Book Now</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>                
        </div>                    
    </section>
    <footer class="gradient-bg p-5" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-4">
                    <h3 class="text-white my-3">About Us</h3>
                    <p class="text-white">Pure Home Real Estate had been established in 2011 with a focus on the real estate industry in the emirate of Abu Dhabi, the capital of the United Arab Emirates.</p>
                </div>
                <div class="col-12 col-sm-6 col-md-4">
                    <h3 class="text-white my-3">Get In Touch</h3>
                    <div><a rel="nofollow" class="text-white " href="#"><i class="fa fa-map-marker"></i>
                    Pure Home Real Estate LLC Al Wahda Complex – Hazza Bin Zayed the First St . P.O. Box: 110388, Abu Dhabi</a></div>
                    <div><a rel="nofollow" class="text-white " href="tel:024466775"><i class="fa fa-phone"></i> +971
                            55 345
                            1555</a></div>
                    <div><a rel="nofollow" class="text-white " href="https://api.whatsapp.com/send?phone=024466775"><i
                                    class="fa fa-whatsapp"></i> 024466775</a>
                    </div>
                    <div><a rel="nofollow" class="text-white " href="mailto:info@purehome-re.ae"><i
                                    class="fa fa-envelope"></i>
                            info@purehome-re.ae</a></div>
                </div>
                <div class="col-12 col-sm-6 col-md-4 final">
                    <h3 class="text-white my-3">Location</h3>
                    <div class="w-100">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14525.528674984955!2d54.36455362929273!3d24.47221191629144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e6795a58f6167%3A0x5ed7237523283b74!2sAl%20Wahda%20Commercial%20Tower!5e0!3m2!1sen!2sae!4v1652115682062!5m2!1sen!2sae" width="100%" height="auto" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
<div id="mobile-footer">
        <div id="nw-ft-whatsapp">
            <div style="height: 60px;
    width: 60px;
    border-radius: 50%;
    border: 0 none;
    padding: 11px;
    background: #398F3B;
    margin-left: 17px;margin-top: -10px;">
                <a href="https://api.whatsapp.com/send?phone=024466775"><img
                            src="https://www.vipcarrental.ae/wp-content/imgs_2021/icon-whatsapp.svg" alt="" style="height: 38px;
    width: 40px;
    border: 0 none;
    padding: 2px 0 0 0;"></a>
            </div>

            <p>WhatsApp</p>
        </div>
        <div id="nw-ft-chat">

            <p>Live Chat</p>
        </div>
        <div id="nw-ft-call">
            <div id="inner-call">
                <div style="height: 60px;
    width: 60px;
    border-radius: 50%;
    border: 0 none;
    padding: 11px;
    background: #398F3B;
    margin-left: 17px;margin-top: -10px;">
                    <a href="tel:024466775"><img
                                src="https://www.vipcarrental.ae/wp-content/imgs_2021/icon-phone.svg" alt="" style="height: 38px;
    width: 40px;
    border: 0 none;
    padding: 4px 0 0 0;"></a>
                </div>

                <p>Call</p>
            </div>
        </div>
    </div>

    <script>
        document.getElementById("nw-ft-chat").onclick = function () {
            Tawk_API.maximize();
            document.getElementById("mobile-footer").classList.add("my-footer-class");
            ;
        };
    </script>
<?php
include_once('include/layout/footer.php');