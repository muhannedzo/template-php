<?php
$URL = './';
?>
<head>
    <meta charset="utf-8"/>
    <title>Pure Home Real Estate</title>
    <meta name="keywords" content="Pure PHP HTML CSS JAVASCRIPT JQUERY"/>
    <meta name="description" content="Pure template created to start any project you want!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link rel="icon" href="./assets/img/pure-logo.png" type="png"/>
    <link href="<?php echo $URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/slick.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/slick-theme.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/intlTelInput.min.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo $URL; ?>assets/css/style.css" rel="stylesheet">
</head>
<header>
    <nav class="navbar navbar-expand-lg  p-0 fixed-top">
        <div class="container px-3 p-md-0">
            <a class="navbar-brand  text-center" href="#"><img src="./assets/img/pure-logo.png" class="nav-logo" type="png"></a>
            <div class="nav-bar-phone d-none d-md-block">
                <a href="tel:+97124466775">
                    <i class="fa fa-phone"></i>
                    024466775
                </a>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars text-white"></i>
            </button>
            <div class="collapse navbar-collapse pl-lg-5" id="navbarNavAltMarkup">
                <div class="navbar-nav text-center">
                    <a class="nav-item nav-link active introNav" href="#intro">Home</a>
                    <a class="nav-item nav-link rentalNav" href="#communities">Communities</a>
                    <a class="nav-item nav-link servicesNav" href="#developers">Developers</a>
                    <a class="nav-item nav-link faqNav" href="#media">Media</a>
                    <a class="nav-item nav-link contactNav" href="#contact">Contact</a>
                </div>

            </div>

        </div>
    </nav>
    <div class="icon-bar">
	  <a href="https://www.instagram.com/pure_home_realestate/" class="instagram" target=".blank"><i class="fa fa-instagram"></i></a> 
	  <a href="https://www.facebook.com/PureHomeRealEstateLLC" class="facebook" target=".blank"><i class="fa fa-facebook-f"></i></a> 
	  <a href="https://twitter.com/PureHome2018" class="twitter" target=".blank"><i class="fa fa-twitter"></i></a> 
	  <a href="https://www.youtube.com/channel/UCzj6l-1iY6givtMZHIdyN1w/videos" class="youtube" target=".blank"><i class="fa fa-youtube-play"></i></a>
	  <a href="http://linkedin.com/in/purehomerealestate" class="linkedin" target=".blank"><i class="fa fa-linkedin"></i></a>
	</div>
</header>
