-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2022 at 07:33 PM
-- Server version: 8.0.29
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_pure`
--

-- --------------------------------------------------------

--
-- Table structure for table `featured_developers`
--

CREATE TABLE `featured_developers` (
  `id` int NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `image_url` varchar(2000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `featured_developers`
--

INSERT INTO `featured_developers` (`id`, `title`, `description`, `image_url`, `created_at`) VALUES
(1, 'Al Dar', 'Aldar Properties is a leading real estate developer in Abu Dhabi and one founded in 2004 by Christopher Sims, the first CEO of the company, and listed to Abu Dhabi market in 2005.  In 2013 the company united with Sorouh Real Estate.', 'https://purehome-re.ae/wp-content/uploads/2021/08/AR-190929096-1.jpg', '2022-05-09 16:30:36'),
(2, 'Imkan', 'IMKAN’S residential, retail, hospitality and commercial projects all have something in common. They aspire to stimulate immediate surroundings and enrich the lives of the people that interact with them.', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-23.jpg', '2022-05-09 16:30:36'),
(3, 'Reportage', 'Reportage properties is a reliable company created by experts in the field of construction, which creates unique projects in Abu Dhabi, ideal for investment for everyone.', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-24.jpg', '2022-05-09 16:30:36'),
(4, 'Profile Group', 'Ever since we started our first project in 2014 in Abu Dhabi at Reportage Group, we have subsequently grown in structure, size and number of projects. Now, Reportage is one the largest private developers in the United Arab Emirates.', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-27.jpg', '2022-05-09 16:30:36'),
(5, 'Manazel', 'Manazel has been successfully operated on UAE real estate market since 2006. It is the company which influence on the Arabian society can’t be underestimated, as it is an outstanding developer, builder and investor.', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-26.jpg', '2022-05-09 16:30:36'),
(6, 'Bloom', 'Since our establishment in 2007, Bloom Holding has risen with bold momentum. Bloom entered the market during a relatively volatile period, poised for challenges and enduring stiff competition.', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-25.jpg', '2022-05-09 16:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `popular_communities`
--

CREATE TABLE `popular_communities` (
  `id` int NOT NULL,
  `title` varchar(2000) DEFAULT NULL,
  `image_url` varchar(3000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `popular_communities`
--

INSERT INTO `popular_communities` (`id`, `title`, `image_url`, `created_at`) VALUES
(1, 'Yas Island', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T094755.905.jpg', '2022-05-09 11:23:16'),
(2, 'Saadyiat Island', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T094907.457.jpg', '2022-05-09 11:23:16'),
(3, 'Aal Reem Island', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T094619.338.jpg', '2022-05-09 11:23:16'),
(4, 'Masdar City', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T095150.192.jpg', '2022-05-09 11:23:16'),
(5, 'Al Maryah Island', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T094157.621.jpg', '2022-05-09 11:23:16'),
(6, 'Al Raha Beach', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T095543.132.jpg', '2022-05-09 11:23:16'),
(7, 'Ghantoot', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T095432.751.jpg', '2022-05-09 11:23:16'),
(8, 'Haydra Village', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T095726.934.jpg', '2022-05-09 11:23:16'),
(9, 'Al Ghadeer', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T095929.285.jpg', '2022-05-09 11:23:16'),
(10, 'The Marina', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-2021-08-29T100600.298.jpg', '2022-05-09 11:23:16');

-- --------------------------------------------------------

--
-- Table structure for table `top_developers`
--

CREATE TABLE `top_developers` (
  `id` int NOT NULL,
  `title` varchar(2000) DEFAULT NULL,
  `image_url` varchar(3000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `top_developers`
--

INSERT INTO `top_developers` (`id`, `title`, `image_url`, `created_at`) VALUES
(1, NULL, 'https://purehome-re.ae/wp-content/uploads/2021/08/Deyaar_-01.png', '2022-05-09 12:35:49'),
(2, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/234.png', '2022-05-09 12:35:49'),
(3, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-29.jpg', '2022-05-09 12:35:49'),
(4, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-31.jpg', '2022-05-09 12:35:49'),
(5, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/New-Project-34.jpg', '2022-05-09 12:35:49'),
(6, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/mag-logo.jpg', '2022-05-09 12:35:49'),
(7, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/unnamed.jpg', '2022-05-09 12:35:49'),
(8, '', 'https://purehome-re.ae/wp-content/uploads/2021/08/i289618.jpg', '2022-05-09 12:35:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `featured_developers`
--
ALTER TABLE `featured_developers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_communities`
--
ALTER TABLE `popular_communities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `top_developers`
--
ALTER TABLE `top_developers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `featured_developers`
--
ALTER TABLE `featured_developers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `popular_communities`
--
ALTER TABLE `popular_communities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `top_developers`
--
ALTER TABLE `top_developers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
